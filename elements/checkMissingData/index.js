// import InformationTitle from "./components/InformationTitle";
import { useDispatch, useSelector } from "react-redux";
// import AvatarsHeader from "./components/AvatarsHeader";
// import DataFillingForm from "./components/DataFillingForm";
import { useEffect, useState } from "react";
import { checkMissingData, textByMissingData } from "@/services/utils/missingDetails";
import Button from "@/elements/common/Button";
// import TimerWithLabel from "./components/common/TimerWithLabel";
import styles from "@/styles/MissingData.module.scss"
import InformationTitle from "@/elements/checkMissingData/components/InformationTitle";
import AvatarsHeader from "@/elements/checkMissingData/components/AvatarsHeader";

const CheckMissingDataForm = ({ setActive, actionSubmit, timer }) => {
  const user = useSelector((state) => state.user.currentUser);
  const project = useSelector(
    (state) => state.projects.currentProject
  );

  const [missingData, setMissingData] = useState([]);

  useEffect(() => {
    const checkData = async () => {
      try {
        const result = await checkMissingData(project, user);
        if (result) setMissingData(result);
        // TODO for checking missingDetails uncomment
        // setMissingData([...missingData,"gender","dob","social"]);
        return true;
      } catch (e) {
        console.log(e);
      }
    };
    checkData();
  }, [project,user]);

  return (
    <div className={styles.missing_container}>
      {user && project && (
        <>
          <AvatarsHeader
            setActive={setActive}
            myAvatar={user?.avatar}
            projectLogo={project?.logo}
            name={user.name}
          />
          {missingData.length === 0 ? (
            <div className={styles.no__missing__data} data-testid="no__missing__data">
              <InformationTitle
                name={project.name}
                message={textByMissingData(
                  missingData,
                  project?.data?.member_mandatory
                )}
              />
              <div className={styles.button__timer__container}>
                {/*{timer && <TimerWithLabel />}*/}
                <Button
                  testId = "button__confirm"
                  wrapperClassName={styles.inputs}
                  text={"CONFIRM"}
                  onClick={() => actionSubmit()}
                />
              </div>
            </div>
          ) : (
            // <DataFillingForm
            //   setActive={setActive}
            //   missingDetails={missingData}
            //   actionSubmit={actionSubmit}
            //   name={project.name}
            //   member_mandatory={project?.data?.member_mandatory}
            // />
            <></>
          )}
        </>
      )}
    </div>
  );
};

export default CheckMissingDataForm;
