import styles from "@/styles/missingData/Components.module.scss"
// import LogoComponent from "./common/LogoComponent";
// import Avatar from "../../../common/Avatar";
import Line from "../../../assets/png/Line 18.png";
import toggleLeft from "../../../assets/arrows/toggleLeft.svg";
import React from "react";
import Button from "@/elements/common/Button";
import Icon from "@/elements/common/Icon";
import Avatar from "@/elements/common/Avatar";
import LogoComponent from "@/elements/checkMissingData/components/common/LogoComponent";

const AvatarsHeader = ({
  myAvatar,
  projectLogo,
  name,
  setActive
}) => {
  return (
    <div className={styles.avatars_container}>
      <Button
        wrapperClassName={styles.button__back}
        isCustomButton
        onClick={() => setActive(false)}
      >
        <Icon src={toggleLeft.src} alt="back" />
      </Button>
      <div className={styles.avatars__line}>
        <div className={styles.avatar__size}>
          <LogoComponent
            avatar={myAvatar}
            avatar_size={"l"}
            name={name}
            className="avatar"
          />
        </div>
        <img className={styles.line} src={Line.src} alt={"line"} />
        <div className={styles.avatar__size}>
          <Avatar size="l" src={projectLogo} alt={"projectLogo"} />
        </div>
      </div>
    </div>
  );
};
export default AvatarsHeader;
