
import React from "react";
import Avatar from "@/elements/common/Avatar";

const LogoComponent = ({ avatar, name, className, avatar_size }) => {
  const getNameInitials = (name) => {
    if (!name) return "";
    const [firstName, lastName] = name.split(" ");
    return `${firstName && firstName[0]}${lastName && lastName[0]}`;
  };

  return (
    <div>
      {avatar ? (
        <Avatar size={avatar_size} src={avatar} alt={"projectLogo"} />
      ) : (
        <div className={"empty " + className}>
          {getNameInitials(name)}
        </div>
      )}
    </div>
  );
};
export default LogoComponent;
