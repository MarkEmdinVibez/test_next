import styles from "@/styles/missingData/Components.module.scss"

const InformationTitle = ({ name, message }) => {
  return (
    <div className={styles.information__title}>
      <div className={styles.information__title__content}>
        <div className={styles.label_container}>
           To connect you with
        </div>
        <div className={styles.title}>{name}</div>
        <div className={styles.label_container} data-testid={"information__message"}>
          {message}
        </div>
      </div>
    </div>
  );
};
export default InformationTitle;
