
const Icon = ({ wrapperClassName, src, alt, style, testId }) => {
  return (
    <div className={wrapperClassName} data-testid={testId}>
      <img src={src} alt={alt} style={style}/>
    </div>
  );
};
export default Icon;