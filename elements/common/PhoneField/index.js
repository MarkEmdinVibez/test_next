import PhoneInput from "react-phone-input-2";
 // import "../../../styles/PhoneField.module.css"
import 'react-phone-input-2/lib/style.css'
import styles from '@/styles/PhoneField.module.css'

const PhoneField = ({ value, setValue, wrapperClassName, customStyle, inputClass, inputStyle, testId }) => {
  //TODO check how to use my custom styles
  return (
    <div
      style={customStyle}
      className={wrapperClassName}
      data-testid={testId}
    >
      <PhoneInput
        countryCodeEditable={false}
        defaultCountry="IL"
        international
        value={value}
        onChange={setValue}
        autoFormat={true}
        inputProps={{
          name: "phone",
          required: true,
          autoFocus: true,
          placeholder: "Phone number",
          autoComplete: "off"
        }}
        inputClass={inputClass}
        inputStyle={inputStyle}
        buttonStyle={{
          background: "white",
          marginLeft: "12px",
          borderWidth: 0
        }}
        dropdownStyle={{
          display: "block",
          textAlign: "start",
          width: "200px",
          color: "black"
        }}
      />
    </div>
  );
};


export default PhoneField;
