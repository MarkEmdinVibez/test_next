
import { buttons } from "@/styles/buttons";

const Button = ({
  text,
  onClick,
  style,
  size,
  commonStyles,
  type,
  wrapperClassName,
  isCustomButton,
  testId,
  children,
  disabled
}) => {
  const handleClick = () => {
    onClick();
  };

  const renderGeneralButton = () => {
    return (
      <div style={commonStyles} className={wrapperClassName}>
        <button
          type={type}
          onClick={handleClick}
          data-testid={testId}
          disabled={disabled || style === "disabled"}
          style={{
            ...buttons.general,
            ...buttons[style],
            ...buttons.size[size]
          }}
        >
          {text}
        </button>
      </div>
    );
  };

  const renderCustomButton = () => {
    return (
      <div className={wrapperClassName} type={type} data-testid={testId} onClick={onClick}>
        {children}
      </div>
    );
  };

  return isCustomButton
    ? renderCustomButton()
    : renderGeneralButton();
};
export default Button;