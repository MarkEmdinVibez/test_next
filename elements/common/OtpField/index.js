import React from "react";
import OtpInput from "react-otp-input";
import styles from "@/styles/Otp.module.css"

const OtpField = ({ value, setValue, error, testId }) => {
  return (
    <div className={styles.container_otp}>
      <OtpInput
        shouldAutoFocus={true}
        renderInput={(props) => <input {...props} />}
        value={value}
        onChange={setValue}
        inputStyle={styles.inputStyle}
        numInputs={4}
        isInputNum={true}
        separator={<span> </span>}
        hasErrored={error}
        errorStyle={{ color: "red" }}
        data-testid={testId}
      />
       <p className={styles.error}>{ error ? (error):""}</p>
    </div>
  );
};

export default OtpField;
