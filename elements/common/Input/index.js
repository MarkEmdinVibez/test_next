import React from "react";
import PropTypes from "prop-types";
import { inputs as input } from "../../../styles/inputs";

const Input = ({
  placeholder,
  wrapperClassName,
  value,
  handleChange,
  onClick,
  onBlur,
  type,
  name,
  id,
  error,
  testId,
  commonStyles,
  label,
  errorNotShown,
  inputClassName
}) => {
  return (
    <div style={commonStyles} className={wrapperClassName}>
      {!!label && <label htmlFor={id}>{label}</label>}
      <input
        autoComplete="off"
        placeholder={placeholder}
        value={value}
        onClick={onClick}
        onChange={handleChange}
        type={type}
        name={name}
        onBlur={onBlur}
        id={id}
        className={`${
          error
            ? `error ${inputClassName}`
            : `${inputClassName}`
        }`}
        data-testid={testId}
      />
      {error && !errorNotShown ? (
        <div>
          <p className="error">{error}</p>
        </div>
      ) : (
        ""
      )}
    </div>
  );
};

Input.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  handleChange: PropTypes.func,
  onBlur: PropTypes.func,
  type: PropTypes.string,
  name: PropTypes.string,
  id: PropTypes.string,
  error: PropTypes.string,
  testId: PropTypes.string,
  commonStyles: PropTypes.object,
  inputStyles: PropTypes.object,
  errorNotShown: PropTypes.bool,
  inputClassName: PropTypes.string
};

Input.defaultProps = {
  placeholder: "",
  value: "",
  handleChange: () => {},
  onBlur: () => {},
  type: "text",
  name: "",
  id: "",
  error: "",
  testId: "",
  commonStyles: {},
  inputStyles: {
    ...input.general,
    ...input.active
  },
  errorNotShown: false,
  inputClassName: ""
};

export default Input;
