

import styles from "@/styles/Avatar.module.css"
import DefaultAvatar from "../../../assets/svg/default_avatar.svg";
import { avatars } from "@/styles/avatars";

const Avatar = ({
  size,
  label,
  marginLeft,
  marginRight,
  margin,
  src,
  labelClassName,
  wrapperClassName
}) => {
  const style = avatars[size];
  return (
    <div className={`${styles.avatar__container} ` + wrapperClassName}>
      <img
        src={src ? src : DefaultAvatar.src}
        alt="avatar"
        style={{ ...style, margin, marginLeft, marginRight }}
      />
      {label && <div className={labelClassName}>{label}</div>}
    </div>
  );
};


export default Avatar;
