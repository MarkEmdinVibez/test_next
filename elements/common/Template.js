import React from "react";
import styles from '@/styles/Template.module.css'

const Template = ({ children, ...props }) => {
  return (
    <div className={`${styles.page} ${props.hasFooter ? `${styles.withFooter}` : ""}`}>
      {children}
    </div>
  );
};

export default Template;
