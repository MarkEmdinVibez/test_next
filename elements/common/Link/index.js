import React from "react";
import styles from '@/styles/CustomLink.module.css'
import { buttons } from "@/styles/buttons";

const CustomLink = ({
  name,
  path,
  type,
  wrapperClassName,
  size,
  skin,
  testId,
  ...props
}) => {
  const style = {
    ...buttons.general,
    ...buttons[skin],
    ...buttons.size[size]
  };

  return (
    <div className={wrapperClassName} data-testid={testId} style={type === "button" ? style : {}}>
      <a href={path}>{props.children ? props.children : name} </a>
    </div>
  );
};


export default CustomLink;
