import { useDispatch, useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import AuthCheck from "@/elements/authCheck/AuthCheck";
import * as api from "@/services/apiCalls/meApi";
import { useCookies } from "react-cookie";
import { getUserData } from "@/redux/user/userActions";


const PrivateRoute = ({ children }) => {
  const { currentUser: user } = useSelector((state) => state.user);
  const [cookies, setCookie, removeCookie] = useCookies();
  const dispatch = useDispatch();

  const [isAuthCheck, setIsAuthCheck] = useState(false);

  useEffect(() => {
    let token = cookies["id"]
    if (token && !user){
      dispatch(getUserData(token));
    }
  }, [user]);

  useEffect(() => {
    checkForAuth();
  }, [user]);

  const checkForAuth = () =>
    !user?.name ? setIsAuthCheck(true) : setIsAuthCheck(false);

  return (
    <>
      {isAuthCheck && <AuthCheck />}
      {children}
    </>
  );
};

export default PrivateRoute;
