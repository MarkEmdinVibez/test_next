import React, { useEffect, useRef, useState } from "react";
import styles from '@/styles/View.module.css'
import { colors } from "@/styles/colors";
import { modalLoginOff, modalLoginOn } from "@/redux/globals/globalActions";
import { useDispatch, useSelector } from "react-redux";
import Button from "@/elements/common/Button";
import { logout } from "@/redux/user/userActions";
import Avatar from "@/elements/common/Avatar";
import Link from "next/link";


const MenuView = ({ toggleHamburger, isOpen }) => {
  const user = useSelector((state) => state.user.currentUser);
  const { token } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const myRef = useRef();

  // TODO use navbar component, remove link styles from here
  // TODO change state of the component when user is logged
  // TODO add trigger for login form

  const activeStyle = { color: colors.white[100] };
  const inActiveStyle = { color: colors.gray[500] };

  const handleClickOutside = (e) => {
    if (!myRef.current.contains(e.target) && !isOpen) return;
    if (!myRef.current.contains(e.target)) toggleHamburger();
  };

  const handleLogout = () => {
    toggleHamburger();
    reset();
    dispatch(modalLoginOff());
  };
  const handleLogin = () => {
    dispatch(modalLoginOn());
  };

  const handleLinkClick = (e) => {
    if (!user) {
      e.preventDefault();
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () =>
      document.removeEventListener("mousedown", handleClickOutside);
  });
  const reset = () => {
    dispatch(logout());
  };

  return (
    <div
      className={`${styles.menu_area} ${isOpen ? `${styles.active}` : ""}`}
      ref={myRef}
    >
      <Avatar src={user?.avatar} size={"s"} />
      {(user?.name || user?.first_name) && (
        <div className={"user_name"}>
          {user?.name
            ? user?.name
            : user?.first_name + " " + user?.last_name}
        </div>
      )}
      <Link
        href={"/tickets"}
        onClick={handleLinkClick}
        style={
          user?.name || user?.first_name ? activeStyle : inActiveStyle
        }
        aria-disabled={!user?.name && !user?.first_name}
      >
        {"Tickets"}
      </Link>
      {user?.name || user?.first_name ? (
        <Button
          onClick={handleLogout}
          isCustomButton
          wrapperClassName={styles.menu_button_disabled}
        >
          <span>Disconnect</span>
        </Button>
      ) : (
        <Button text={"CONNECT"} size={"s"} onClick={handleLogin} />
      )}
    </div>
  );
};
export default MenuView;
