import { useEffect, useState } from "react";
import MenuView from "./menu/View";
import styles from '@/styles/View.module.css'
import HamburgerIcon from "../../assets/svg/hamburger.svg";
import Icon from "@/elements/common/Icon";

const Hamburger = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleHamburger = () => setIsOpen(!isOpen);
  return (
    <div className={`${styles.menu_container} ${isOpen ? `${styles.active}` : ""}`}>
      <MenuView toggleHamburger={toggleHamburger} isOpen={isOpen} />
      {!isOpen && (
        <div onClick={toggleHamburger} className={styles.menu_button}>
          <Icon src={HamburgerIcon.src} />
        </div>
      )}
    </div>
  );
};
export default Hamburger;
