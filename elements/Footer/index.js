import Container from "../common/Container";
import styles from '@/styles/Foter.module.css'
import Icon from "@/elements/common/Icon";
import AppStore from "../../assets/svg/appStore.svg";
import GooglePlay from "../../assets/svg/googlePlay.svg";
import Logo from "../../assets/svg/footerLogo.svg";
import Point from "../../assets/svg/point.svg";
import VibezImage from "../../assets/png/vibez.png";
import CustomLink from "@/elements/common/Link";
const text = [
  "Access your tickets (even offline)",
  "Get event updates",
  "Pay cashless at the event"
];

const Footer = ({ withImage = false, wrapperClassName }) => {
  const renderPoint = (text) => {
    return (
      <div className="styles.point" key={text}>
        <Icon src={Point.src} wrapperClassName="styles.point__icon" />
        <div>{text}</div>
      </div>
    );
  };

  const renderImage = () => {
    return (
      <div className={styles.footer__image}>
        <div className={styles.footer__image_container}>
          <img src={VibezImage.src} alt="Vibez" />
        </div>

        <div className={styles.footer__image_content}>
          <div className={styles.footer__image_text}>JOIN YOUR TRIBE</div>
          <CustomLink
            path={"/"}
            wrapperClassName={styles.footer__image_logo}
          >
            <img src={Logo.src} alt="logo" />
          </CustomLink>
        </div>
      </div>
    );
  };

  return (
    <Container className={styles.centrified}>
      <div
        className={`${styles.footer} ${
          withImage ? styles.with_image : ""
        } ${wrapperClassName}`}
      >
        {withImage && renderImage()}
        <div className={styles.footer__container}>
          <div className={styles.footer__header}>
            <div>GET VIBEZ APP</div>
            <CustomLink path={"/"} wrapperClassName={styles.footer__logo}>
              <img src={Logo.src} alt="logo" />
            </CustomLink>
          </div>
          <div className={styles.footer__content}>
            {text.map((string) => renderPoint(string))}
          </div>

          <div className={styles.links_wrapper}>
            <CustomLink
              path="https://play.google.com/store/apps/details?id=vibez.io"
              wrapperClassName={styles.storeLink}
            >
              <img src={GooglePlay.src} alt="Google play" />
            </CustomLink>
            <CustomLink
              path="https://apps.apple.com/il/app/vibez-io/id6444364738"
              wrapperClassName={styles.storeLink}
            >
              <img src={AppStore.src} alt="App Store" />
            </CustomLink>
          </div>
          <CustomLink
            name="Producer? Create your event, boost your community"
            wrapperClassName={styles.producer_link}
            path="https://backoffice.vibez.io/"
          />
        </div>
      </div>
    </Container>
  );
};

export default Footer;
