import { fonts } from "../styles/fonts";
import styles from '@/styles/ToGo.module.css'

const ToGo = ({
  wrapperClassName,
  timeStyle,
  textStyle,
  testIdDiv,
  testIdTitle,
  testIdSubtitle,
  numOfDays,
  option
}) => {
  const options = {
    live: {
      title: "Live",
      subtitle: "Now"
    },
    future: {
      title: numOfDays,
      subtitle: "Days to go"
    },
    ended: {
      title: "ENDED",
      subtitle: ""
    }
  };


  return (
    <div data-testid={testIdDiv} className={`${styles.toGo} ` + wrapperClassName + ` ${option}`}>
      <p
        style={{
          ...fonts.bold,
          ...timeStyle
        }}
        data-testid={testIdTitle}
      >
        {options[option].title}
      </p>
      <p data-testid={testIdSubtitle} style={{ ...fonts.regular, ...textStyle }}>
        {options[option].subtitle}
      </p>
    </div>
  );
};

export default ToGo;
