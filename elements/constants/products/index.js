import AccommodationTicket from "../../../assets/icons/desktop/accommodation_ticket.svg";
import AccommodationTicketMobile from "../../../assets/icons/mobile/accommodation_ticket.svg";
import ChildTicket from "../../../assets/icons/desktop/child_ticket.svg";
import ChildTicketMobile from "../../../assets/icons/mobile/child_ticket.svg";
import DrinkTicket from "../../../assets/icons/desktop/drink_ticket.svg";
import DrinkTicketMobile from "../../../assets/icons/mobile/drink_ticket.svg";
import TransportTicket from "../../../assets/icons/desktop/transport_ticket.svg";
import TransportTicketMobile from "../../../assets/icons/mobile/transport_ticket.svg";
import MerchandiseTicket from "../../../assets/icons/desktop/merchandise.svg";
import MerchandiseTicketMobile from "../../../assets/icons/mobile/merchandise.svg";
import OtherTicket from "../../../assets/icons/desktop/other_ticket.svg";
import OtherTicketMobile from "../../../assets/icons/mobile/other_ticket.svg";
import EntryTicket from "../../../assets/icons/desktop/entry_ticket.svg";
import EntryTicketMobile from "../../../assets/icons/mobile/entry_ticket.svg";

export const PRODUCT_TYPE_DICT = {
  entry_ticket: {
    title: "Entry ticket",
    icon: EntryTicket,
    mobileIcon: EntryTicketMobile
  },
  child_ticket: {
    title: "Child ticket",
    icon: ChildTicket,
    mobileIcon: ChildTicketMobile
  },
  transport_ticket: {
    title: "Transport ticket",
    icon: TransportTicket,
    mobileIcon: TransportTicketMobile
  },
  merchandise: {
    title: "Merchandise",
    icon: MerchandiseTicket,
    mobileIcon: MerchandiseTicketMobile
  },
  drinks_and_food: {
    title: "Drinks & Food",
    icon: DrinkTicket,
    mobileIcon: DrinkTicketMobile
  },
  accommodation: {
    title: "Accommodation",
    icon: AccommodationTicket,
    mobileIcon: AccommodationTicketMobile
  },
  other: {
    title: "Other",
    icon: OtherTicket,
    mobileIcon: OtherTicketMobile
  }
};
