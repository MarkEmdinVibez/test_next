import PublicEvent from "../../../assets/svg/publicEvent.svg";
import PrivateEvent from "../../../assets/svg/privateEvent.svg";
import MembersOnly from "../../../assets/svg/membersOnly.svg";
import PublicEventEnded from "../../../assets/svg/publicEventDisabled.svg";
import PrivateEventEnded from "../../../assets/svg/privateEventDisabled.svg";
import MembersOnlyEnded from "../../../assets/svg/membersOnlyDisabled.svg";

export const EVENT_TYPES = {
  free: {
    active: PublicEvent,
    ended: PublicEventEnded
  },
  manual: {
    active: PrivateEvent,
    ended: PrivateEventEnded
  },
  members: { active: MembersOnly, ended: MembersOnlyEnded }
};
