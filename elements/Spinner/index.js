import BeatLoader from "react-spinners/BeatLoader";
import styles from "@/styles/Spinner.module.css";

const LoadingSpinner = () => {
  return (
    <div className={styles.spinner__container}>
      <BeatLoader color="#5855FF" />
    </div>
  );
};

export default LoadingSpinner;
