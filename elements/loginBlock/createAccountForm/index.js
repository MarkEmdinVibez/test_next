import React, { useCallback } from "react";
import { Field, Form, Formik } from "formik";
import * as Yup from "yup";
import { t } from "i18next";
import { useDispatch } from "react-redux";
import Button from "../../common/Button";
import footer from "../../../assets/backgrounds/footer.svg";
import styles from "@/styles/LoginBlock.module.css"
import { logUserSuccess } from "@/redux/user/userActions";
import { updateMe } from "@/services/apiCalls/meApi";
import { useCookies } from "react-cookie";
import Input from "@/elements/common/Input";

const CreateAccount = () => {
  const [cookies, setCookie, removeCookie] = useCookies();
  const dispatch = useDispatch();
  const SignupSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, "Too Short!")
      .test("name length", "At least 2 words", (value) => {
        const hebrewPattern = /^[\u0590-\u05FF\s]+$/;
        const englishPattern = /^[A-Za-z\s]+$/;

        if (hebrewPattern.test(value) || englishPattern.test(value)) {
          const words = value.trim().split(/\s+/);
          if (words.length >= 2) return true;
        }

        return false;
      })
      .required("Required"),
    email: Yup.string().email("Invalid email").required("Required")
  });

  const handleFormSubmit = useCallback(
    async (values) => {
      try {
        const id = cookies["id"];
        const result = await updateMe(values, id);
        dispatch(logUserSuccess(result.person));
      } catch (e) {
        console.log(e);
      }
    },
    [dispatch]
  );

  const buttonStatusCheck = (errors, touched) =>
    Object.keys(touched).length === 0 ||
    Object.keys(errors).length !== 0;

  return (
    <div className={styles.container_form}>
      <Formik
        initialValues={{ name: "", email: "" }}
        validationSchema={SignupSchema}
        onSubmit={handleFormSubmit}
      >
        {({ errors, touched, handleBlur, handleChange, values }) => (
          <Form className={styles.create__account__form}>
            <div className={styles.create__account__title}>
              {t("translation:create_account.title")}
            </div>
            <div className={styles.field__container}>
              <div className={styles.label_container}>
                <label> {t("translation:input.name_label")}</label>
              </div>
              <Field
                component={Input}
                type="text"
                name="name"
                id="name"
                testId="name"
                onBlur={handleBlur}
                placeholder={t("translation:input.name_placeholder")}
                value={values.name}
                error={touched.name ? errors.name : ""}
                handleChange={handleChange}
              />
            </div>
            <div className={styles.field__container}>
              <div className={styles.label_container}>
                <label> {t("translation:input.email_label")}</label>
              </div>
              <Field
                component={Input}
                type="text"
                name="email"
                id="email"
                testId="email"
                onBlur={handleBlur}
                placeholder={t("translation:input.email_placeholder")}
                value={values.email}
                error={touched.email ? errors.email : ""}
                handleChange={handleChange}
              />
            </div>
            <div className={styles.button__submit}>
              <Button
                type="submit"
                size="l"
                commonStyles={{ "margin-top": "15px" }}
                style={
                  buttonStatusCheck(errors, touched)
                    ? "disabled"
                    : "active"
                }
                text={t("translation:cta.create_account")}
              />
            </div>
          </Form>
        )}
      </Formik>
      <div className={styles.footer_image}>
        <img src={footer.src} alt="footer" width="500" height="166" />
      </div>
    </div>
  );
};

export default CreateAccount;
