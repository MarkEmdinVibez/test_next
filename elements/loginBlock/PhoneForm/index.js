import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { t } from "i18next";
import Button from "../../common/Button";
import PhoneField from "../../common/PhoneField";
import Icon from "../../common/Icon";
import toggleLeft from "../../../assets/arrows/toggleLeftWithMargin.svg";
import logoWithLabelLeft from "../../../assets/svg/logoWithLabelLeft.svg";
// import { useLocation, useNavigate } from "react-router-dom";
import footer from "../../../assets/backgrounds/footer.svg";
import styles from "@/styles/LoginBlock.module.css"
import CustomLink from "../../common/Link";
import { createMe, getVerificationCode } from "@/services/apiCalls";
import { handlePress } from "@/services/utils/helpers";
import { modalLoginOff } from "@/redux/globals/globalActions";
const PhoneForm = ({ changeCurrentForm }) => {
  // const navigate = useNavigate();
  // const location = useLocation();
  const [error, setError] = useState("")
  const [phone, setPhone] = useState("+972");
  const dispatch = useDispatch();

  const phoneHandleSubmit = async () => {
    try {
      const result = await createMe({ os: "Browser" });
      localStorage.setItem("id", result.person.id);
      await getVerificationCode("+" + phone, result.person.id);
      changeCurrentForm(false, phone);
      setError("");
    } catch (e) {
      console.log(e);
      setError("Error");
    }
  };

  return (
    <div
      className={styles.container_form}
      onKeyDown={(e) => handlePress(e, phoneHandleSubmit)}
    >
      <div className={styles.back_button}>
        {/*<Button*/}
        {/*  isCustomButton*/}
        {/*  onClick={() => {*/}
        {/*    if (*/}
        {/*      location.pathname.endsWith("products") ||*/}
        {/*      location.pathname.endsWith("tickets")*/}
        {/*    )*/}
        {/*      navigate(-1);*/}
        {/*    dispatch(modalLoginOff());*/}
        {/*  }}*/}
        {/*>*/}
        {/*  <Icon src={toggleLeft} alt="back" />*/}
        {/*</Button>*/}
      </div>
      <div className={styles.container_form__content}>
      <h2>{t("translation:phone_verification.title")}</h2>
      <label>{t("translation:phone_verification.text_1")}</label>
      <label>{t("translation:phone_verification.text_2")}</label>
      <div className="phone__input">
        <PhoneField
          value={phone}
          setValue={setPhone}
          inputClass={error ? (`${styles.phone__style} ${styles.error}`) : (styles.phone__style)}
        />
      </div>
      <Button
        wrapperClassName="button_phone_submit"
        text={t("translation:cta.send_code")}
        onClick={phoneHandleSubmit}
        size="l"
        style={phone.length > 4 ? "active" : "disabled"}
      />
      </div>
      <div className={styles.phone__footer__content}>
      <div className={styles.logo}>
        <Icon src={logoWithLabelLeft.src} alt="Vibez" />
      </div>
      <div className={styles.label__private}>
        <label>
          By signing up you agree to our
          <CustomLink
            name="Privacy policy"
            path="https://backoffice.vibez.io/#/auth/licenses"
            wrapperClassName={styles.private__color}
          />
          and
          <CustomLink
            name="Terms of use"
            path="https://backoffice.vibez.io/#/auth/licenses"
            wrapperClassName={styles.private__color}
          />
        </label>
      </div>
      </div>
      <div className={styles.footer_image}>
        <img src={footer.src} alt="footer" width="500" height="120" />
      </div>
    </div>
  );
};
export default PhoneForm;
