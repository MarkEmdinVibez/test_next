import React, { useState } from "react";

import PhoneForm from "../PhoneForm";
import OtpForm from "@/elements/loginBlock/OtpForm";

function LoginForm() {
  const [phone, setPhone] = useState("+972");
  const [visiblePhoneForm, setVisiblePhoneForm] = useState(true);

  const changeCurrentForm = (visiblePhone, phone) => {
    setVisiblePhoneForm(visiblePhone);
    setPhone(phone);
  };

  return (
    <>
      {visiblePhoneForm ? (
        <PhoneForm changeCurrentForm={changeCurrentForm} />
      ) : (
        <OtpForm phone={phone} setVisiblePhoneForm={setVisiblePhoneForm}/>
      )}
    </>
  );
}

export default LoginForm;
