import { useDispatch, useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import { t } from "i18next";
import resendVector from "../../../assets/svg/Vector 4.svg";
import toggleLeft from "../../../assets/arrows/toggleLeftWithMargin.svg";
import footer from "../../../assets/backgrounds/footer.svg";
import styles from "@/styles/LoginBlock.module.css"
import Button from "@/elements/common/Button";
import Icon from "@/elements/common/Icon";
import { getVerificationCode } from "@/services/apiCalls";
import { delay, handlePress } from "@/services/utils/helpers";
import { login, logUserFailure } from "@/redux/user/userActions";
import OtpField from "@/elements/common/OtpField";
import { useCookies } from "react-cookie";

const OtpForm = ({ phone, setVisiblePhoneForm }) => {
  const error = useSelector((state) => state.user.error);
  const [otpCode, setOtpCode] = useState("");
  const [resendCode, setResendCode] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(logUserFailure(""));
  },[]);

  const handleResendCode = async () => {
    if (resendCode) {
      try {
        setResendCode(false);
        await getVerificationCode("+" + phone,localStorage.getItem("id"));
        await delay(5000);
        setResendCode(true);
      } catch (e) {
        dispatch(logUserFailure(e.message()));
      }
    }
  };
  const handleChangeOtpCode = (value) => {
    setOtpCode(value);
    dispatch(logUserFailure(""));
    if (value.length === 4) {
      dispatch(login(value));
    }
  };

  const codeHandleSubmit = async () => {
    dispatch(login(otpCode));
  };

  return (
    <div
      className={styles.container_form}
      onKeyDown={(e) => handlePress(e, codeHandleSubmit)}
    >
      <div className={styles.back_button}>
        <Button
          isCustomButton
          onClick={() => setVisiblePhoneForm(true)}
        >
          <Icon src={toggleLeft} alt="back" />
        </Button>
      </div>
      <div className={styles.container_form__content}>
      <div className={styles.label__otp}>
        <label>{t("translation:otp.text")}</label>
        <label> +{phone} </label>
      </div>
      <OtpField
        value={otpCode}
        setValue={handleChangeOtpCode}
        error={error}
        testId="otp"
      />
      <Button
        wrapperClassName={styles.button_phone_submit}
        text={t("translation:cta.continue")}
        onClick={codeHandleSubmit}
        size="l"
        style={!error && otpCode.length == 4 ? "active" : "disabled"}
      />
      <div className={styles.resend__code}>
        <label onClick={handleResendCode}>
          {t("translation:cta.resend_code")}
        </label>
        {!resendCode && <img src={resendVector.src} alt="sent" />}
      </div>
      </div>
      <div className={styles.footer_image_otp}>
        <img src={footer.src} alt="footer" width="500" height="120" />
      </div>
    </div>
  );
};

export default OtpForm;
