import styles from "@/styles/ticketsFolder/UsedComponent.module.scss"
import { showDate, showTime } from "@/services/utils/time";


const UsedComponent = ({ used, scanDate }) => {
  return (
    <div className={styles.used__container} data-testid="used__item">
      <div className={styles.used__title}>{used ? ("USED"):("CANCELED")}</div>
      { scanDate
        && (
      <div className={styles.used__date}>{`${showDate(scanDate
      )} ${showTime(
        scanDate
      )}`}
      </div>)
      }
    </div>
  )
}

export default UsedComponent