
import QRCode from "react-qr-code";
import { useEffect, useState } from "react";
import ClaimNewTicket from "../claimNewTicket";
import UsedComponent from "../usedComponnent";
import Avatar from "@/elements/common/Avatar";
import Icon from "@/elements/common/Icon";
import { PRODUCT_TYPE_DICT } from "@/elements/constants/products";
import { showDateFull, showTime } from "@/services/utils/time";
import  styles  from "@/styles/ticketsFolder/Ticket.module.scss"

const Item = ({
  ticket,
  user,
  deleteItem,
  setActiveMissingData,
  setCurrentItem,
  setHighlightTicket
}) => {
  const [activeEvent, setActiveEvent] = useState(true);
  const [assignedItem, setAssignedItem] = useState(false);
  const [myItem, setMyItem] = useState(false);
  const [claimedItem, setClaimedItem] = useState(false);
  const [newItem, setNewItem] = useState(false);
  const [pending, setPending] = useState(false);
  const [used, setUsed] = useState(false);
  const [canceled, setCanceled] = useState(false);

  useEffect(() => {
    isEventFinished();
    isItMyItem();
    isItAssignedItem();
    isItClaimedItem();
    isItNewItem();
    isItPending();
    isItUsedItem();
    isItCanceledItem();
  });

  const isEventFinished = () => {
    const currentDate = new Date();
    const endEventDate = new Date(ticket?.data?.event?.end_time);
    if (endEventDate > currentDate) {
      setActiveEvent(true);
    } else {
      setActiveEvent(false);
    }
  };

  const isItAssignedItem = () =>
    ticket?.person?.id !== ticket?.data?.order?.person?.id
      ? setAssignedItem(true)
      : setAssignedItem(false);

  const isItMyItem = () =>
    ticket.person?.id === user?.id
      ? setMyItem(true)
      : setMyItem(false);

  const isItUsedItem = () =>
    ticket?.used || ticket?.scanned_at
      ? setUsed(true)
      : setUsed(false);

  const isItCanceledItem = () =>
    ticket?.status === "canceled"
      ? setCanceled(true)
      : setCanceled(false);

  const isItPending = () =>
    ticket?.data?.order?.state === "review_pending"
      ? setPending(true)
      : setPending(false);

  const isItClaimedItem = () =>
    ticket.status === "claimed"
      ? setClaimedItem(true)
      : setClaimedItem(false);

  const isItNewItem = () =>
    ticket.status === "new"
      ? setNewItem(true)
      : setNewItem(false);

  const greyBackground = () => !activeEvent || used || canceled;

  const showContent = () => {
    if (used || canceled)
      return (
        <UsedComponent used={used} scanDate={ticket?.scanned_at} />
      );

    if (pending && !(newItem && myItem))
      return (
        <div className={styles.pending__container} data-testid="pending__container">PENDING APPROVAL</div>
      );

    if (myItem && newItem)
      return (
        <ClaimNewTicket
          deleteItem={deleteItem}
          ticket={ticket}
          setActiveMissingData={setActiveMissingData}
          setCurrentTicket={setCurrentItem}
        />
      );

    return <QRCode value={ticket?.qr_code} className={styles.qr__code} />;
  };

  return (
    <div
      data-testid="item"
      className={styles.my__items__container}
      onMouseOver={(e) => {
        setHighlightTicket(ticket);
      }}
    >
      <div className={styles.items__logo}>
        <Avatar
          size="m"
          wrapperClassName={styles.logo__header}
          src={ticket?.data?.project?.logo}
        />
      </div>
      <div
        className={`${styles.items__content}  ${
          greyBackground() ? `${styles.ended__event}` : `${styles.active__event}`
        }`}
      >
        <Icon
          src={
            PRODUCT_TYPE_DICT[ticket?.data?.product?.product_type]?.icon.src
          }
          wrapperClassName={styles.product__type__icon}
        />
        <div className={styles.text__content}>
          <div className={styles.product__name} data-testid="product__name">
            {ticket?.data?.product?.name}
          </div>
          <div className={styles.event__name} data-testid="event__name">
            {ticket?.data?.event?.name?.toUpperCase()}
          </div>
          <div className={styles.product__info} data-testid="product__info__time">{`${showDateFull(
            ticket?.data?.event?.start_time
          )} | ${showTime(ticket?.data?.event?.start_time)}`}</div>
          <div className={styles.product__info} data-testid="product__info__location">
            {ticket?.data?.event?.location}
          </div>
        </div>
        <div className={styles.items__owner}>
          <Avatar size="xs" src={ticket?.person?.avatar} />
          <div className={styles.owner__name} data-testid="owner__name">
            {ticket?.person?.name
              ? ticket?.person?.name.toUpperCase()
              : ticket?.person?.phone.toUpperCase()}
          </div>
          {myItem && assignedItem && (
            <div className={styles.assigned__info} data-testid="assigned__info">
              {`Assigned to you by ${ticket?.data?.order?.person?.name}`}
            </div>
          )}
        </div>
        <div className={styles.qr__content}>{showContent()}</div>
        {!myItem && !greyBackground() && (
          <div data-testid="claimed__ticket"
            className={`${styles.claimed__ticket} ${
              claimedItem ? `${styles.claimed}` : `${styles.not__claimed}`
            }`}
          >
            {claimedItem ? "CLAIMED" : "NOT CLAIMED"}
          </div>
        )}
      </div>
    </div>
  );
};

export default Item;
