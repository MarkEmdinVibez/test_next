
import styles from "@/styles/ticketsFolder/ClaimNewTicket.module.scss";
// import { updateMyItem } from "../../../../services/apiCalls";
import { useDispatch } from "react-redux";
import { updateMyItem } from "@/services/apiCalls/myItemsApi";
import { fetchProject } from "@/redux/projects/projectsActions";
import Button from "@/elements/common/Button";
// import { fetchProject } from "../../../../redux/projects/projectsActions";

const ClaimNewTicket = ({
  deleteItem,
  ticket,
  setActiveMissingData,
  setCurrentTicket
}) => {
  const dispatch = useDispatch();

  const declineAction = async () => {
    try {
      const data = {
        status: "declined"
      };
      const result = await updateMyItem(ticket.id, data);
      deleteItem(ticket);
    } catch (e) {
      console.log(e);
    }
  };

  const claimAction = () => {
    dispatch(fetchProject(ticket?.data?.project?.id));
    setCurrentTicket(ticket);
    setActiveMissingData(true);
  };

  return (
    <div className={styles.buttons__container}data-testid={"new_item"}>
      <Button text="CLAIM" size="s" onClick={claimAction} testId={"claim_button"}/>
      <Button
        text="DECLINE"
        size="s"
        wrapperClassName={styles.button__decline}
        style={"warning"}
        testId={"decline_button"}
        onClick={declineAction}
      />
    </div>
  );
};

export default ClaimNewTicket;
