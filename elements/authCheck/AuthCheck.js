import { useSelector } from "react-redux";
import React from "react";
import LoadingSpinner from "@/elements/Spinner";
import Modal from "@/elements/modal/Modal";
import LoginForm from "@/elements/loginBlock/LoginForm";
import CreateAccount from "@/elements/loginBlock/createAccountForm";


const AuthCheck = () => {
  const {
    loading,
    currentUser: user,
  } = useSelector((state) => state.user);
  console.log("AuthCheck",user);

  return (
    <>
      {loading ? (
        <LoadingSpinner/>
      ) : (
        <div>
          {!user?.name  && (
            <Modal active={true}>
              {user ? <CreateAccount /> : <LoginForm />}
            </Modal>
          )}
        </div>
      )}
    </>
  );
};
export default AuthCheck;
