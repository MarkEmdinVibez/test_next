import React from "react";
import styles from "@/styles/Modal.module.css"
import { fonts } from "@/styles/fonts";

const Modal = ({ active, children, testId }) => {
  return (
    <div
      className={active ? `${styles.modal} ${styles.active}` : styles.modal}
      style={{ ...fonts.light }}
      data-testid={testId}
    >
        {children}
    </div>
  );
};
export default Modal;
