import { useCallback, useEffect, useState } from "react";
import Container from "@/elements/common/Container";
import styles from '@/styles/EventHeader.module.css'
import Button from "@/elements/common/Button";
import { useRouter } from "next/router";
import ToGo from "@/elements/ToGo";
import Icon from "@/elements/common/Icon";
import { EVENT_TYPES } from "@/elements/constants/event";

const EventHeader = ({ event, items=[], isStickyHeader }) => {
  const router = useRouter()
  const [option, setOption] = useState("future");
  const [isClicking, setIsClicking] = useState(false);
  const [numOfDays, setNumOfDays] = useState(0);

  const animate = () => {
    setIsClicking(true);
    setTimeout(() => setIsClicking(false), 1000);
  };
  const navigateMain = () => {
    router.push(`/events/${event?.id}/products`)
  }

  const getDetails = useCallback(() => {
    const ended =
      new Date(
        event?.end_time ? event?.end_time : event?.start_time
      ) < new Date();
    const isLive = new Date(event?.start_time) < new Date();

    if (ended) {
      return setOption("ended");
    }

    if (isLive) {
      setOption("live");
    } else {
      const diffInMs = new Date(event?.start_time) - new Date();
      const num = diffInMs / (1000 * 60 * 60 * 24);
      if (num) setNumOfDays(Math.round(num));
    }
  }, [event]);

  useEffect(() => {
    if (event) getDetails();
  }, [event, getDetails]);
  const { cover, name, id, event_type } = event;
  const eventType = EVENT_TYPES[event_type];

  return (
    <div
      className={`${styles.event__header_container} ${
        isStickyHeader ? `${styles.isSticky}` : ""
      }`}
    >
      <div
        className={`${styles.event__stickyHeader} ${
          isStickyHeader ? `${styles.active}` : `${styles.inActive}`
        }`}
      >
        <Container className={styles.event__stickyHeader_wrapper}>
          <div className={styles.event__stickyHeader_titleContainer}>
            {/*<ToGo*/}
            {/*  option={option}*/}
            {/*  event={event}*/}
            {/*  numOfDays={numOfDays}*/}
            {/*  wrapperClassName="event__stickyHeader_togo"*/}
            {/*  timeStyle={{*/}
            {/*    fontSize: "18px",*/}
            {/*    lineHeight: "21.69px"*/}
            {/*  }}*/}
            {/*  textStyle={{*/}
            {/*    fontSize: "12px",*/}
            {/*    lineHeight: "14.46px"*/}
            {/*  }}*/}
            {/*/>*/}
            <div className={styles.event__stickyHeader_title}>
              {event?.name}
            </div>
          </div>
          {/*<div className={styles.event__stickyHeader_button}>*/}
          {/*  <CustomLink*/}
          {/*    path={`/events/${id}/products`}*/}
          {/*    skin="active"*/}
          {/*    type="button"*/}
          {/*    name="GET TICKETS"*/}
          {/*    wrapperClassName="event__header_link"*/}
          {/*  />*/}
          {/*</div>*/}
        </Container>
      </div>
      <div
        className={`${styles.event__general}`}
      >
        <div
          className={styles.event__header_background_blur}
          style={{ backgroundImage: `url(${cover})` }}
        ></div>
        <div className={styles.event__header_background}>
          <Container className={styles.headerImage}>
            <div className={styles.event__header_wrapper}>
              <div className={styles.event__header_image}>
                <img src={cover} alt={name} />
              </div>
              <div className={styles.event__header_title}>{name}</div>
            </div>
          </Container>
        </div>
      </div>
      {!isStickyHeader && (
        <Container className={styles.event__container}>
          <div className={styles.event__header_linkWrapper}>
            <Button
              size="l"
              text={option === "ended" ? "ENDED" : "GET TICKETS"}
              disabled={option === "ended"}
              style={option === "ended" ? "disabled" : "active"}
              onClick={() => navigateMain()}
              type="button"
              wrapperClassName={styles.event__header_link}
              testId={"button_header"}
            />
          </div>
          <div className="event__header_details">
            <ToGo
              numOfDays={numOfDays}
              testIdDiv={"days_to_go_background"}
              testIdTitle={"days_to_go_title"}
              testIdSubtitle={"days_to_go_subtitle"}
              option={option}
              event={event}
              wrapperClassName={styles.event__header_togo}
              timeStyle={{ fontSize: "18px", lineHeight: "21.69px" }}
              textStyle={{ fontSize: "12px", lineHeight: "14.46px" }}
            />
            <Icon
              src={
                option === "ended"
                  ? eventType?.ended.src
                  : eventType?.active.src
              }
            />
            {/*<PreviewTickets tickets={items} />*/}
          </div>
        </Container>
      )}
    </div>
  );
};
export default EventHeader