
async function SendRequest(method, end_point, body) {
  console.log("method",method);
  console.log("end_point",end_point);
  console.log("body",body);

  try {
    const response = await fetch(
      process.env.NEXT_PUBLIC_REACT_APP_API_URL + end_point,
      {
        method: method,
        headers: {
          "Content-Type": "application/json",
        },
        body: body
      }
    );

    const res = await response.json();

    if (res?.error) {
      throw new Error(res.error.message);
    }

    return res;
  } catch (e) {
    throw new Error(e);
  }
}

async function UploadRequest(end_point, body) {
  try {
    const response = await fetch(
      process.env.NEXT_PUBLIC_REACT_APP_API_URLREACT_APP_API_URL + end_point,
      {
        method: "POST",
        body: body
      }
    );
    if (response.ok) {
      return response;
    } else {
      throw new Error("UPLOAD ERROR");
    }
  } catch (e) {
    throw new Error(e);
  }
}
export { SendRequest, UploadRequest };
