import { SendRequest } from "./api";
import { GET_EVENT_TIME_OUT, EVENTS_PATH, GET_PRODUCTS_TIME_OUT } from "./constants";
import { cacheController } from "./controllers/cacheController";

async function getEvent(event_id) {
  let end_point = EVENTS_PATH + event_id;
  let res = await SendRequest("GET", end_point);
  // let res = await cacheController(GET_EVENT_TIME_OUT,"GET", end_point);
  if (res) return res;
}

async function getEvents() {
  let end_point = EVENTS_PATH;
  let res = await SendRequest("GET", end_point);
  if (res) return res;
}

async function getEventProducts(event_id) {
  let end_point = EVENTS_PATH + event_id + "/products";
  let res = await cacheController(GET_PRODUCTS_TIME_OUT,"GET", end_point);
  if (res) return res;
}

export { getEvent, getEventProducts, getEvents };
