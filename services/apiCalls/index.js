/*******************PROJECTS******************/
import {
  getProject,
  getProjects,
  getProjectUpcomingEvents,
  getProjectEvents,
  joinProject,
  leaveProject
} from "./projectsApi";
/*******************EVENTS******************/
import { getEvent, getEventProducts } from "./eventsAPI";
/*******************ME******************/
import { getVerificationCode, createMe, verifyPhone } from "./meApi";
/*******************TICKETS******************/

export {
  getProject,
  getProjects,
  getProjectUpcomingEvents,
  getProjectEvents,
  joinProject,
  leaveProject,
  getEvent,
  getEventProducts,
  getVerificationCode,
  createMe,
  verifyPhone
};
