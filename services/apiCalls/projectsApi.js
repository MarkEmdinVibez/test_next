import { SendRequest } from "./api";
import { GET_PROJECT_TIME_OUT, PROJECTS_PATH as project_path } from "./constants";
import { cacheController } from "./controllers/cacheController";

async function getProject(project_id) {
  let end_point = project_path + "/" + project_id;
  let res = await cacheController(GET_PROJECT_TIME_OUT,"GET", end_point);
  //let res = await SendRequest("GET", end_point);
  if (res) return res;
}
//brings only upcoming events
async function getProjects() {
  let end_point = project_path + "?upcoming_events=true";
  let res = await SendRequest("GET", end_point);
  if (res) return res;
}
//brings only upcoming events
async function getProjectUpcomingEvents(project_id) {
  let end_point =
    project_path + "/" + project_id + "?upcoming_events=true";
  let res = await SendRequest("GET", end_point);
  if (res) return res;
}
//brings all evens
async function getProjectEvents(project_id) {
  let end_point = project_path + "/" + project_id + "/events";
  let res = await SendRequest("GET", end_point);
  if (res) return res;
}
async function joinProject(project_id) {
  let end_point = project_path + "/" + project_id + "/membership";
  let res = await SendRequest("POST", end_point);
  if (res) return true;
}
async function leaveProject(project_id) {
  let end_point = project_path + "/" + project_id + "/membership";
  let res = await SendRequest("DELETE", end_point);
  if (res) return true;
}

export {
  getProject,
  getProjects,
  getProjectUpcomingEvents,
  getProjectEvents,
  joinProject,
  leaveProject
};
