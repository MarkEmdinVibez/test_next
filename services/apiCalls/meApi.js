import { SendRequest, UploadRequest } from "./api";
import { FIND_PERSON_TIME_OUT, GET_ME_TIME_OUT, ME_PATH_, PHONE_PATH } from "./constants";
import { normalizePhoneNumber } from "@/services/utils/helpers";

async function getVerificationCode(phone_num, user_id) {
  const endpoint = "/me/" + user_id + PHONE_PATH;

  let body = JSON.stringify({
    person: {
      phone:  normalizePhoneNumber(phone_num)
    }
  });
  let res = await SendRequest("PUT", endpoint, body);
  if (res) return res;
}

async function createMe({ token, os, os_version, iso }) {
  let body = JSON.stringify({
    person: {
      device_attributes: null
    }
  });
  let res = await SendRequest("POST", "/me/", body);
  if (res) return res;
}

async function getMe() {
  let end_point = await ME_PATH_();
  // let res = await cacheController(GET_ME_TIME_OUT,"GET", end_point);
  let res = await SendRequest("GET", end_point);
  if (res) return res;
}

async function updateMe(data) {
  let end_point = await ME_PATH_();
  let body = JSON.stringify({
    person: data
  });
  let res = await SendRequest("PUT", end_point, body);
  if (res) return res;
}

async function verifyPhone(code, user_id) {
  let end_point = "/me/" + user_id + "?verification_code=" + code;
  let res = await SendRequest("GET", end_point);
  if (res) return res;
}

const findPerson = async (phone) => {
  let end_point = await ME_PATH_() + `/find_person?phone=${normalizePhoneNumber(phone)}`
  let res = await SendRequest("GET", end_point);
  // let res = await cacheController(FIND_PERSON_TIME_OUT,"GET", end_point);
  if (res) return res;
}

const uploadImage = async (data) => {
  let end_point = await ME_PATH_() + "/avatar";
  let res = await UploadRequest(end_point, data);
  if (res) return res;
};

export {
  verifyPhone,
  getVerificationCode,
  createMe,
  getMe,
  updateMe,
  uploadImage,
  findPerson
};
