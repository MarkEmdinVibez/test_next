import { SendRequest } from "./api";
import { GET_ALL_MY_ITEMS_TIME_OUT, GET_MY_ITEMS_TIME_OUT } from "./constants";
import { cacheController } from "./controllers/cacheController";

export const MY_ITEMS_PATH_ = async (id) => "/me/" + id + "/items";

async function getMyItems() {
  const end_point = await MY_ITEMS_PATH_();
  // let res = await SendRequest("GET", end_point);
  let res = await cacheController(GET_MY_ITEMS_TIME_OUT,"GET", end_point);
  if (res) return res;
}

async function getAllMyItems(user_id) {
  const end_point = await MY_ITEMS_PATH_(user_id) + "?all=true" ;
  // let res = await cacheController(GET_ALL_MY_ITEMS_TIME_OUT,"GET", end_point);
  let res = await SendRequest("GET", end_point);
  if (res) return res;
}

async function getMyItem(item_id) {
  const end_point = await MY_ITEMS_PATH_() + "/" + item_id;
  let res = await SendRequest("GET", end_point);
  if (res) return res;
}

async function updateMyItem(item_id, data) {
  let end_point = await MY_ITEMS_PATH_() + "/" + item_id;
  let body = JSON.stringify({
    item: data
  });
  let res = await SendRequest("PUT", end_point, body);
  if (res) return res;
}

export { getMyItems, updateMyItem, getMyItem, getAllMyItems };
