// import { getCookie } from "../utils/cookies";
// import { getMemberships } from "./membershipsApi";
// import { getPaymentMethods } from "./paymentMethodsApi";
import cookieCutter from 'cookie-cutter'
//
//
export const ME_PATH_ = async () => {
  const userId = cookieCutter.get("id") || "";
  return "/me/" + userId;
}
//
// export const ORDER_PATH_ = async () => await ME_PATH_() + "/orders";
// export const MY_ITEMS_PATH_ = async () => await ME_PATH_() + "/items";
// export const PAYMENT_METHODS_ = async () => await ME_PATH_() + "/payment_methods";
// export const NOTIFICATIONS_PATH_ = async () => await ME_PATH_() + "/notifications";

export const EVENTS_PATH = "/events/";
export const ITEMS_PATH = "/items";
export const PROJECTS_PATH = "/projects";
export const PHONE_PATH = "/phone_verification";

export const GET_EVENT_TIME_OUT = 20;
export const GET_PRODUCTS_TIME_OUT = 1;
export const GET_PROJECT_TIME_OUT = 20;
export const GET_ME_TIME_OUT = 20;
export const GET_MY_ITEMS_TIME_OUT = 20;
export const GET_ALL_MY_ITEMS_TIME_OUT = 20;
export const FIND_PERSON_TIME_OUT = 20;
export const GET_PAYMENT_METHODS_TIME_OUT = 20;
export const GET_MEMBERSHIPS_TIME_OUT = 20;