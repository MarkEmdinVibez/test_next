
import * as api from "../ordersApi";

async function sortOrderItems(order_id) {
  let { items } = await api.getOrderItems(order_id);

  items.sort(
    (x, y) =>
      new Date(x.created_at) -
      new Date(y.created_at)
  );
  return items;
}
export { sortOrderItems }