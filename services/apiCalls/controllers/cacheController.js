import { SendRequest } from "../api";

const customSendRequest = async (method, end_point, body) => {
  let res = await SendRequest("GET", end_point);
  res.casheTime = new Date();
  await setToCache(end_point, res);
  return res;
};

const setToCache = async (name, data) => {
  caches
    .open("vibez_cache")
    .then((cache) => {
      const jsonResponse = new Response(JSON.stringify(data), {
        headers: {
          "content-type": "application/json"
        }
      });
      cache.put(name, jsonResponse);
    })
    .catch((err) => {
      console.log(err);
    });
};

const cacheController = async (
  timeForUpdate,
  method,
  end_point,
  body
) => {
  try {
    const cache = await caches.open("vibez_cache");
    const cachedResponse = await cache.match(end_point);
    if (cachedResponse) {
      const resp = await cachedResponse.json();
      const currentTime = new Date();
      const dateCache = new Date(resp.casheTime);
      const diffTime = Math.floor((currentTime - dateCache) / 1000);
      if (diffTime > timeForUpdate) {
        return customSendRequest(method, end_point, body);
      } else {
        return resp;
      }
    } else {
      return customSendRequest(method, end_point, body);
    }
  } catch (e) {
    console.log(e);
  }
};

export { cacheController };
