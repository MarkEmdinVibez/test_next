import { MEMBER_MANDATORY_NAMES } from "@/services/utils/dictionaries";


const checkMissingData = async (project, user) => {
  if (!project?.data?.member_mandatory) return false;
  if (!user) return false;
  if (Object.keys(project?.data?.member_mandatory).length === 0)
    return false;
  let missingData = [];
  let keys = Object.keys(project?.data?.member_mandatory);
  for (let i = 0; i < keys.length; i++) {
    if (project?.data?.member_mandatory[keys[i]] === false) continue;
    if (keys[i] === "social") {
      if (user.facebook != null || user.instagram != null) {
        continue;
      } else {
        missingData.push(keys[i]);
      }
    } else if (user[keys[i]] == null) missingData.push(keys[i]);
  }
  return missingData;
};

const textByMissingData = (missingData, member_mandatory) => {
  /// TODO add in dictionary
  let textDefaultPart_1 =
    "we’re going to share your full name, phone number,";
  let textDefaultPart_2 = " and email with them";

  let arrOfMandatory = [];
  for (let element in member_mandatory) {
    if (member_mandatory[element] === true) {
      arrOfMandatory.push(element);
    }
  }
  if (missingData.length !== 0) {
    let s = new Set(missingData);
    arrOfMandatory = arrOfMandatory.filter((e) => !s.has(e));
    textDefaultPart_2 =
      textDefaultPart_2 + ", but a few details are still missing:";
  }
  arrOfMandatory.forEach((element) => {
    textDefaultPart_1 =
      textDefaultPart_1 + ` ${MEMBER_MANDATORY_NAMES[element]},`;
  });
  return textDefaultPart_1 + textDefaultPart_2;
};

const missingDataButtonStatus = (
  errors,
  touched,
  missingDetails,
  setActiveButton
) => {
  let numberOfItems = missingDetails.length;
  let currentActiveItems = 0;
  const errKeys = Object.keys(errors);
  const touchedKeys = Object.keys(touched);
  missingDetails.forEach((element) => {
    if (element === "social") {
      if (errKeys.includes("social")) {
        let socialErrorKeys = Object.keys(errors.social);

        if (!socialErrorKeys.includes("facebook")) {
          currentActiveItems = currentActiveItems + 1;
        }
        if (!socialErrorKeys.includes("instagram")) {
          currentActiveItems = currentActiveItems + 1;
        }
      } else {
        if (touchedKeys.includes("social")) {
          currentActiveItems = currentActiveItems + 1;
        }
      }
    } else {
      if (
        !errKeys.includes(element) &&
        touchedKeys.includes(element)
      ) {
        currentActiveItems = currentActiveItems + 1;
      }
    }
  });
  if (
    currentActiveItems === numberOfItems &&
    !errKeys.includes("social")
  ) {
    setActiveButton(true);
    return "COMPLETED";
  } else {
    setActiveButton(false);
  }
  return currentActiveItems + "/" + numberOfItems + " COMPLETED";
};

export {
  checkMissingData,
  textByMissingData,
  missingDataButtonStatus
};
