import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import  dict_ru  from "./ru/dict.json";
import  dict_en  from "./en/dict.json";
import  dict_de  from "./de/dict.json";

// the translations
// (tip: move them in a JSON file and import them)

i18n
    .use(initReactI18next)
    .init({
        compatibilityJSON: "v3", //prevent crash on Android
        fallbackLng: 'en',
        debug: true,
        // defaultNS: "translation",
        interpolation: {
            escapeValue: false,
        }, //React already does escaping
        resources: {
            en: {
                translation: dict_en,
            },
            de: {
                translation: dict_de,
            },
            ru: {
                translation: dict_ru,
            }
        }
    });
export default i18n;