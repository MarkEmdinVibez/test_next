import { format } from "date-fns";

export function showDateFull(date) {
  if (!date) return null;
  return format(new Date(date), "eee, d MMM yyyy");
}

export function showDate(date) {
  if (!date) return null;
  return format(new Date(date), "d MMM, yyyy");
}

export function showTime(date) {
  if (!date) return null;
  return format(new Date(date), "HH:mm");
}

export const formateTime = ({ date, time }) => {
  const now = new Date().getTime();
  const countDownDate = new Date(date).getTime();
  const distance = countDownDate + time * 60 * 1000 - now; //15 minutes

  let minutes = Math.floor(
    (distance % (1000 * 60 * 60)) / (1000 * 60)
  );
  let seconds = Math.floor((distance % (1000 * 60)) / 1000);

  if (minutes < 10) minutes = "0" + minutes;
  if (seconds < 10) seconds = "0" + seconds;

  return `${minutes}:${seconds}`;
};

export function isPast(datetime) {
  if (!datetime) return null;
  return new Date(datetime) < new Date();
}
