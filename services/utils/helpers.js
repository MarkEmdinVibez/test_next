const handlePress = (e, action) =>
  e.keyCode === 13 ? action() : null;

const copyEventLink = async () => {
  try {
    if (window.isSecureContext && navigator.clipboard){
      await navigator.clipboard.writeText(window.location.href);
    }
  } catch (e) {
    console.log(e);
  }
};
const delay = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};


// const normalizeEventLink = (id) => {
//   if (
//     window.location.protocol === "http:" &&
//     window.location.hostname !== "localhost"
//   ) {
//     window.location.protocol = "https:";
//   }
//   if(id?.length > 36){
//     window.location.href = window.location.origin + "/events/" + id.slice(0,36);
//   }
// }
const deleteZeroFromNumber = (phone) =>
  phone.includes("+9720") ? (phone.slice(0,4) + phone.slice(5)) : (phone);

const normalizePhoneNumber = (phone) =>
  phone.includes("+") ? (deleteZeroFromNumber(phone)) : (deleteZeroFromNumber("+" + phone));

const correctEventId = (id) =>
   id.match(/[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}/gm)


export { handlePress, copyEventLink, correctEventId, delay, normalizePhoneNumber };
