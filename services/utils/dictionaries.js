export const MEMBER_MANDATORY_NAMES = {
  gender: "gender",
  dob: "date of birth",
  social: "social media account",
  avatar: "avatar"
};

export const GENDER_OPTIONS = [
  { label: "Woman", value: "0" },
  { label: "Man", value: "1" },
  { label: "Transgender", value: "2" },
  { label: "Non-binary/non-conforming", value: "3" },
  { label: "Prefer not to respond", value: "4" }
];

export const MONTH = {
  "01": "Jan",
  "02": "Feb",
  "03": "Mar",
  "04": "Apr",
  "05": "May",
  "06": "Jun",
  "07": "Jul",
  "08": "Aug",
  "09": "Sep",
  "10": "Oct",
  "11": "Nov",
  "12": "Dec",
}

