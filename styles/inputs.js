import { colors } from "./colors";

export const inputs = {
  inActive: {
    color: colors.white[100],
    backgroundColor: colors.gray[900]
  },
  active: {
    color: colors.black[900],
    backgroundColor: colors.white[100]
  },
  error: {
    color: colors.pink[500],
    backgroundColor: colors.white[100]
  },
  missingData: {
    color: colors.pink[500],
    backgroundColor: colors.gray[900]
  },
  missingDataValid: {
    color: colors.white[100],
    backgroundColor: colors.gray[900]
  },
  general: {
    height: "66px",
    width: "100%",
    padding: `21px 23px`,
    borderRadius: "6px",
    fontSize: "16px",
    boxSizing: "border-box"
  },
  card: {
    height: "40px",
    display: "flex",
    justifyContent: "center",
    padding: "8px 0"
  },
  cardSize: {
    regular: {
      width: "455px"
    },
    cvv: {
      width: "90px"
    },
    expireDate: {
      width: "138px"
    }
  }
};
