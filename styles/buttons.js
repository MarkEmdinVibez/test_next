
import { gradients } from "./gradients";
import { fonts } from "./fonts";
import { colors } from "@/styles/colors";

export const buttons = {
  general: {
    borderRadius: "30px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    border: "none",
    cursor: "pointer",
    ...fonts.medium
  },
  disabled: {
    color: colors.gray[300],
    backgroundColor: colors.gray[700]
  },
  warning: {
    color: colors.white[100],
    background: gradients.warning
  },
  active: {
    color: colors.white[100],
    background: gradients.active
  },
  event: {
    color: colors.white[100],
    background: gradients.event
  },
  size: {
    l: {
      width: "340px",
      height: "50px",
      fontSize: "18px",
      fontWeight: "bold"
    },
    m: {
      width: "240px",
      height: "40px",
      fontSize: "16px"
    },
    s: {
      width: "120px",
      height: "30px",
      fontSize: "12px"
    }
  }
};
