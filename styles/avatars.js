export const avatars = {
  xs: {
    width: "32px",
    height: "32px",
    borderRadius: "50%"
  },
  s: {
    width: "42px",
    height: "42px",
    borderRadius: "50%"
  },
  m: {
    width: "54px",
    height: "54px",
    borderRadius: "50%"
  },
  l: {
    width: "86px",
    height: "86px",
    borderRadius: "50%"
  },
  xl: {
    width: "140px",
    height: "140px",
    borderRadius: "50%"
  }
};
