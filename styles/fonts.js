export const fonts = {
  bold: {
    fontFamily: "JostBold",
    fontWeight: "bold"
  },
  regular: {
    fontFamily: "JostRegular",
  },
  medium: {
    fontFamily: "JostMedium",
  },
  light: {
    fontFamily: "JostLight",
  }
};
