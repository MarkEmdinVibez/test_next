import { colors } from "@/styles/colors";

export const gradients = {
  warning: `linear-gradient(90deg, ${colors.pink[900]}, ${colors.red[500]})`,
  active: `linear-gradient(90deg, ${colors.blue[700]}, ${colors.blue[400]})`,
  event: `linear-gradient(90deg, ${colors.green[300]}, ${colors.blue[200]})`,
  ticketActive: `linear-gradient(90deg, ${colors.blue[200]}, ${colors.purple[200]})`,
  ticketDisabled: `linear-gradient(246.68deg, ${colors.gray[100]}, ${colors.gray[800]})`,
  product: `linear-gradient(180deg, ${colors.gray[700]}, ${colors.gray[900]})`
};
