import "@/styles/globals.css";
import { Provider } from "react-redux";
import store from "@/redux/store";
import { CookiesProvider } from "react-cookie";
import { I18nextProvider } from "react-i18next";
// import * as Sentry from "@sentry/react";
// import { BrowserTracing } from "@sentry/tracing";
import i18n from "../services/utils/translations/i18n";

// Sentry.init({
//   dsn: "https://cb1750672ed94f88830edb25a760982a@o1289712.ingest.sentry.io/4504786347753472",
//   integrations: [new BrowserTracing()],
//
//   // We recommend adjusting this value in production, or using tracesSampler
//   // for finer control
//   tracesSampleRate: 1.0,
// });

export default function App({ Component, pageProps }) {
  return (
    <>
      <I18nextProvider i18n={i18n}>
        <Provider store={store}>
          <CookiesProvider>
            <Component {...pageProps} />
          </CookiesProvider>
        </Provider>
      </I18nextProvider>
    </>
  );
}
