import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import Template from "@/elements/common/Template";
import Hamburger from "@/elements/hamburger";
import { useDispatch, useSelector } from "react-redux";
import AuthCheck from "@/elements/authCheck/AuthCheck";
import { useEffect, useRef } from "react";
import { getUserData } from "@/redux/user/userActions";
import { useCookies } from "react-cookie";

const inter = Inter({ subsets: ['latin'] })

export default function Home({ children }) {
  const loginModal = useSelector((state) => state.globals.loginModal);
  const [cookies, setCookie, removeCookie] = useCookies();
  const dispatch = useDispatch();
  const shouldGetData = useRef(true);
  // const { token } = useSelector((state) => state.user);

  useEffect(() => {
    let token = cookies["id"]
    if (shouldGetData.current) {
      if (token) {
        shouldGetData.current = false
        dispatch(getUserData(token));
      }
    }
  }, [dispatch]);
  return (
    <Template>
      {loginModal && <AuthCheck />}
      <Hamburger />
      {children}
    </Template>
  )
}
