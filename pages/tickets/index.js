import React, { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import ScrollContainer from "react-indiana-drag-scroll";
import styles from "@/styles/TicketsPage.module.scss"
import FooterBackground from "../../assets/backgrounds/purple.svg";
import {
  getAllMyItems,
  updateMyItem
} from "@/services/apiCalls/myItemsApi";
import Template from "@/elements/common/Template";
import LoadingSpinner from "@/elements/Spinner";
import Modal from "@/elements/modal/Modal";
import PrivateRoute from "@/elements/PrivateRoute";
import Home from "@/pages";
import Item from "@/elements/TicketPage/ticket";
import CheckMissingDataForm from "@/elements/checkMissingData";


const AllTicketsPage = () => {
  const shouldGetData = useRef(true);
  const [items, setItems] = useState([]);
  const [currentItem, setCurrentItem] = useState({});
  const [highlightTicket, setHighlightTicket] = useState({});
  const [activeMissingData, setActiveMissingData] = useState(false);
  const [loading, setLoading] = useState(false);

  const { currentUser: user, token } = useSelector(
    (state) => state.user
  );

  useEffect(() => {
    const fetchAllItems = async (user_id) => {
      try {
        const result = await getAllMyItems(user_id);
        const filteredItems = result?.items?.filter(el => new Date(el.data?.event?.end_time) > new Date());
        filteredItems.sort(
          (x, y) =>
            new Date(x.data?.event?.start_time) -
            new Date(y.data?.event?.start_time)
        );
        setHighlightTicket(filteredItems[0]);
        setItems(filteredItems);
        setLoading(false);
      } catch (e) {
        setLoading(false);
        console.log(e);
      }
    };
    if(shouldGetData.current && token){
      shouldGetData.current = false;
      setLoading(true);
      user && fetchAllItems(user?.id);
    }
  }, [user]);

  const deleteItem = (item) => {
    const arrTickets = [...items];
    const myIndex = arrTickets.indexOf(item);
    arrTickets.splice(myIndex, 1);
    setItems(arrTickets);
  };

  const actionSubmitMissingData = async () => {
    try {
      const data = {
        status: "claimed"
      };
      const result = await updateMyItem(currentItem.id, data);
      const arrTickets = [...items];
      const item = arrTickets.find((x) => x.id === currentItem.id);
      item.status = "claimed";
      setItems(arrTickets);
      setActiveMissingData(false);
    } catch (e) {
      console.log(e);
      setActiveMissingData(false);
    }
  };

  return (
    <Home>
    <PrivateRoute>
    <Template>
      {loading ? (
        <LoadingSpinner />
      ) : (
        <>
          {activeMissingData && (
            <Modal active={activeMissingData}>
              <CheckMissingDataForm
                setActive={setActiveMissingData}
                actionSubmit={actionSubmitMissingData}
                timer={false}
              />
            </Modal>
          )}
          <div className={styles.items__page__container}>
            <div className={styles.title}>YOUR TICKETS</div>
            {items.length !== 0 ? (
              <div className={styles.items_with_dots}>
                <ScrollContainer
                  className={styles.items__container}
                  vertical={false}
                >
                  {items.map((ticket) => (
                    <Item
                      key={ticket.id}
                      ticket={ticket}
                      user={user}
                      setHighlightTicket={setHighlightTicket}
                      deleteItem={deleteItem}
                      setCurrentItem={setCurrentItem}
                      setActiveMissingData={setActiveMissingData}
                    />
                  ))}
                </ScrollContainer>
                {/*<ScrollingDots*/}
                {/*  items={items}*/}
                {/*  highlight={highlightTicket}*/}
                {/*/>*/}
              </div>
            ) : (
              <div className={styles.no_tickets}>
                <div className={styles.title_no_tickets}>
                  No vibez here yet :(
                </div>
                <div className={styles.subtitle_no_tickets}>
                  Your tickets to upcoming events will appear here
                </div>
              </div>
            )}
            <div className={styles.tickets__footer}>
              <img src={FooterBackground.src} alt="" />
            </div>
          </div>
        </>
      )}
    </Template>
      </PrivateRoute>
    </Home>
  );
};

export default AllTicketsPage;
