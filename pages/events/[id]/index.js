import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import styles from "@/styles/Event.module.css";
import Container from "@/elements/common/Container";
import EventHeader from "@/elements/EventHeader";
import FooterBackground from "../../../assets/backgrounds/purple.svg";
import { useSelector, useDispatch } from "react-redux";
import Home from "@/pages";
import Button from "@/elements/common/Button";
import Footer from "@/elements/Footer";
import * as api from "../../../services/apiCalls/eventsAPI";
import { getEvent } from "@/redux/events/eventsActions";
import { useCookies } from "react-cookie";

export const getServerSideProps = async (context) => {
  const { id } = context.params;
  const { event } = await api.getEvent(id);

  if (!event) {
    return {
      notFound: true
    };
  }
  return {
    props: {
      event_data: event
    }
  };
};

const Event = ({ event_data }) => {
  const router = useRouter();
  const [cookies, setCookie, removeCookie] = useCookies();

  const dispatch = useDispatch();
  const {
    currentEvent: event,
    loading,
    error
  } = useSelector((state) => state.events);

  useEffect(() => {
    console.log("tut",event_data);
    if (event_data) {
      dispatch(getEvent(event_data));
      // setCookie(
      //   event_data?.name,
      //   event_data?.name, {
      //   path: `/events/${event_data.id}`,
      // });
    }
  }, [event_data]);

  const endedEvent = () =>
    new Date(event?.end_time ? event?.end_time : event?.start_time) <
    new Date();

  return (
    <Home>
      {event && (
        <div className={styles.eventPage}>
          <EventHeader
            event={event}
            items={[]}
            isStickyHeader={false}
          />
          <Container
            className={`${styles.event__container} ${styles.event} `}
          >
            <div className={styles.event__title}>
              {event?.name.toUpperCase()}
            </div>
            <div className={styles.event__details}>
              <div>
                {/*<p>*/}
                {/*  {`${showDateFull(event?.start_time)} | ${showTime(*/}
                {/*    event?.start_time*/}
                {/*  )}`}*/}
                {/*</p>*/}
                <p>{event?.location}</p>
              </div>
            </div>
            <div className={styles.event__description}>
              {event?.description}
            </div>
          </Container>
          <Footer wrapperClassName={styles.footer__size} />
          <div className={styles.event__footer}>
            <img src={FooterBackground} alt="" />
            <div className={styles.event__footer_content}>
              <Button
                text={endedEvent() ? "ENDED" : "GET TICKETS"}
                disabled={endedEvent()}
                style={endedEvent() ? "disabled" : "active"}
                // onClick={() =>
                //   navigate(`/events/${event?.id}/products`)
                // }
                type="button"
                wrapperClassName={styles.event__footer_link}
                testId="button_footer"
              />
            </div>
          </div>
        </div>
      )}
    </Home>
  );
};

export default Event;
