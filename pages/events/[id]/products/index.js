import PrivateRoute from "@/elements/PrivateRoute";

const Index = () => {
  return (
    <PrivateRoute>
      <div> Product Page </div>
    </PrivateRoute>
  );
};
export default Index;
