import PrivateRoute from "@/elements/PrivateRoute";

const AssignPage = () => {
  return (
    <PrivateRoute>
      <div> AssignPage </div>
    </PrivateRoute>
  );
};
export default AssignPage;
