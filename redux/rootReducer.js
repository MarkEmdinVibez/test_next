import { combineReducers } from "redux";
import { user } from "./user/userReducer";
import { projects } from "./projects/projectsReducer";
import { myItems } from "./myItems/myItemsReducer";
import { events } from "./events/eventsReducer"
import { globals } from "./globals/globalReducer";

const rootReducer = combineReducers({
  user,
  projects,
  myItems,
  events,
  globals,
});

export default rootReducer;
