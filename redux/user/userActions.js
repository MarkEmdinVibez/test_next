import * as api from "../../services/apiCalls/meApi";
import cookieCutter from 'cookie-cutter'

import {
  LOG_OUT,
  LOG_USER_FAILURE,
  LOG_USER_REQUEST,
  LOG_USER_SUCCESS
} from "../constants";
import { useCookies } from "react-cookie";
import { USER_ID_TIME } from "@/elements/constants/time";
// import {
//   deleteCookie,
//   setCookie
// } from "../../services/utils/cookies";
// import { USER_ID_TIME } from "../../constants/time";

export const logout = () => async (dispatch) => {
  cookieCutter.set('id', '', { expires: new Date(0) })

  dispatch({
    type: LOG_OUT
  });
};

export const logUserRequest = () => ({
  type: LOG_USER_REQUEST
});

export const logUserSuccess = (user) => ({
  type: LOG_USER_SUCCESS,
  payload: user
});

export const logUserFailure = (error) => ({
  type: LOG_USER_FAILURE,
  payload: error
});

export const login = (code) => async (dispatch) => {

  try {
    const id = localStorage.getItem("id");
    const { person } = await api.verifyPhone(code, id);
    cookieCutter.set("id", person.id,{
      secure: true,
      "max-age": USER_ID_TIME * 60 * 60 * 24
    })
    localStorage.removeItem("id");
    dispatch(logUserSuccess(person));
  } catch (error) {
    dispatch(logUserFailure(error.message));
  }
};

export const getUserData = (id) => async (dispatch) => {
  dispatch(logUserRequest());

  try {
    const result = await api.getMe(id);
    dispatch(logUserSuccess(result.person));
  } catch (error) {
    dispatch(logUserFailure(error.message));
  }
};
