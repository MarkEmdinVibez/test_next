import {
  LOG_OUT,
  LOG_USER_FAILURE,
  LOG_USER_SUCCESS,
  LOG_USER_REQUEST
} from "../constants";
import cookieCutter from 'cookie-cutter'

// const getToken = () => {
//   let result = cookieCutter.get("id");
//   return result
// }


const initialState = {
  currentUser: null,
  loading: false,
  error: "",
  token: null
};

export const user = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case LOG_OUT:
      return {
        ...state,
        token: null,
        currentUser: null
      };
    case LOG_USER_SUCCESS:
      return {
        ...state,
        token: payload.id,
        currentUser: payload,
        loading: false
      };
    case LOG_USER_REQUEST:
      return {
        ...state,
        loading: true,
        error: ""
      };
    case LOG_USER_FAILURE:
      return {
        ...state,
        loading: false,
        error: payload
      };
    default:
      return state;
  }
};
