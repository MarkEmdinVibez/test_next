import * as api from "../../services/apiCalls/myItemsApi";
import {
  FETCH_ALL_MY_ITEMS_SUCCESS,
  FETCH_MY_ITEM_SUCCESS,
  FETCH_MY_ITEM_FAILURE,
  FETCH_MY_ITEM_REQUEST,
  RESET_MY_ITEMS
} from "../constants";

const fetchMyItemRequest = () => ({
  type: FETCH_MY_ITEM_REQUEST
});

const fetchMyItemSuccess = (project) => ({
  type: FETCH_MY_ITEM_SUCCESS,
  payload: project
});

const fetchAllMyItemsSuccess = (projects) => ({
  type: FETCH_ALL_MY_ITEMS_SUCCESS,
  payload: projects
});

export const fetchMyItemFailure = (error) => ({
  type: FETCH_MY_ITEM_FAILURE,
  payload: error
});

export const getMyItem = (id) => async (dispatch) => {
  dispatch(fetchMyItemRequest());

  try {
    const res = await api.getMyItem(id);

    dispatch(fetchMyItemSuccess(res.ticket));
  } catch (error) {
    dispatch(
      fetchMyItemFailure(error.message || "Event is sold out")
    );
  }
};

export const resetMyItems = () => ({
  type: RESET_MY_ITEMS,
  payload: {
    loading: false,
    error: "",
    myItems: [],
    currentMyItem: null,
    currentMyItemId: null
  }
});

export const getMyItems = () => async (dispatch) => {
  dispatch(fetchMyItemRequest());

  try {
    const { items } = await api.getMyItems();

    dispatch(fetchAllMyItemsSuccess(items));
  } catch (error) {
    dispatch(fetchMyItemFailure(error.message));
  }
};
