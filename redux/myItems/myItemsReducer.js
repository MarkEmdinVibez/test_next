import {
  FETCH_ALL_MY_ITEMS_SUCCESS,
  FETCH_MY_ITEM_SUCCESS,
  FETCH_MY_ITEM_FAILURE,
  FETCH_MY_ITEM_REQUEST,
  RESET_MY_ITEMS
} from "../constants";

const initialState = {
  loading: false,
  error: "",
  myItems: [],
  currentMyItem: null,
  currentMyItemId: null
};

export const myItems = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_ALL_MY_ITEMS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: "",
        myItems: payload
      };
    case FETCH_MY_ITEM_SUCCESS:
      return {
        ...state,
        loading: false,
        error: "",
        currentMyItem: payload,
        currentMyItemId: payload.id
      };
    case FETCH_MY_ITEM_REQUEST:
      return {
        ...state,
        loading: true,
        error: ""
      };
    case FETCH_MY_ITEM_FAILURE:
      return {
        ...state,
        loading: false,
        error: payload
      };
    case RESET_MY_ITEMS:
      return {
        ...state,
        ...payload
      };
    default:
      return state;
  }
};
