import {
  FETCH_EVENTS_REQUEST,
  FETCH_EVENT_SUCCESS,
  FETCH_EVENT_PRODUCTS_SUCCESS,
  FETCH_EVENTS_FAILURE,
  UNSET_EVENT,
  FETCH_ALL_EVENT_SUCCESS
} from "../constants";

const initialState = {
  loading: false,
  error: "",
  currentEvent: null,
  currentEventId: null,
  products: null,
  events: []
};

export const events = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_EVENT_SUCCESS:
      return {
        ...state,
        loading: false,
        error: "",
        currentEvent: payload,
        currentEventId: payload?.id
      };
    case FETCH_EVENTS_REQUEST:
      return {
        ...state,
        loading: true,
        error: ""
      };
    case FETCH_EVENTS_FAILURE:
      return {
        ...state,
        loading: false,
        error: payload
      };
    case UNSET_EVENT:
      return {
        ...state,
        currentEvent: null,
        currentEventId: null
      };
    case FETCH_EVENT_PRODUCTS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: "",
        products: payload
      };
    case FETCH_ALL_EVENT_SUCCESS: 
    return {
      ...state,
      events: payload,
      loading: false,
      error: ""
    };
    default:
      return state;
  }
};
