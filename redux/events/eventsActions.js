import * as api from "../../services/apiCalls/eventsAPI";
import {
  FETCH_EVENTS_FAILURE,
  FETCH_EVENT_SUCCESS,
  FETCH_EVENTS_REQUEST,
  UNSET_EVENT,
  FETCH_EVENT_PRODUCTS_SUCCESS,
  FETCH_ALL_EVENT_SUCCESS
} from "../constants";
import { fetchProject } from "../projects/projectsActions";

const fetchEventsRequest = () => ({
  type: FETCH_EVENTS_REQUEST
});

const fetchAllEventsSuccess = (events) => ({
  type: FETCH_ALL_EVENT_SUCCESS,
  payload: events
});

const fetchEventSuccess = (event) => ({
  type: FETCH_EVENT_SUCCESS,
  payload: event
});

const fetchEventProductsSuccess = (products) => ({
  type: FETCH_EVENT_PRODUCTS_SUCCESS,
  payload: products
});

export const fetchEventsFailure = (error) => ({
  type: FETCH_EVENTS_FAILURE,
  payload: error
});

export const unsetEvent = () => ({
  type: UNSET_EVENT,
  payload: null
});

export const getEvent = (event_data) => async (dispatch, getState) => {
  dispatch(fetchEventSuccess(event_data));
  // dispatch(fetchEventsRequest());
  // const { currentProject } = getState().projects;
  //
  // try {
  //   const { event } = await api.getEvent(id);
  //
  //   if (currentProject?.id !== event?.project?.id) {
  //     dispatch(fetchProject(event?.project.id));
  //   }
  //
  //   dispatch(fetchEventSuccess(event));
  // } catch (error) {
  //   dispatch(fetchEventsFailure(error.message));
  // }
};

export const getProducts = (eventId) => async (dispatch) => {
  dispatch(fetchEventsRequest());

  try {
    const { products } = await api.getEventProducts(eventId);
    dispatch(fetchEventProductsSuccess(products));
  } catch (error) {
    dispatch(fetchEventsFailure(error.message));
  }
};

export const fetchEvents = () => async (dispatch) => {
  dispatch(fetchEventsRequest());

  try {
    const { events } = await api.getEvents();

    dispatch(fetchAllEventsSuccess(events));
  } catch (error) {
    dispatch(fetchEventsFailure(error.message));
  }
};
