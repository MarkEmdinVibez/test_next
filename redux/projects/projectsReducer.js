import {
  FETCH_ALL_PROJECTS_SUCCESS,
  FETCH_PROJECT_SUCCESS,
  FETCH_PROJECT_FAILURE,
  FETCH_PROJECT_REQUEST,
  UNSET_PROJECT
} from "../constants";

const initialState = {
  loading: false,
  error: "",
  projects: [],
  currentProject: null,
  currency: "ILS"
};

export const projects = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_ALL_PROJECTS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: "",
        projects: payload
      };
    case FETCH_PROJECT_SUCCESS:
      return {
        ...state,
        loading: false,
        error: "",
        currentProject: payload,
        // currency: payload.currency
      };
    case FETCH_PROJECT_REQUEST:
      return {
        ...state,
        loading: true,
        error: ""
      };
    case FETCH_PROJECT_FAILURE:
      return {
        ...state,
        loading: false,
        error: payload
      };
    case UNSET_PROJECT:
      return {
        ...state,
        currentProject: null
      };
    default:
      return state;
  }
};
