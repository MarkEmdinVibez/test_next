import * as api from "../../services/apiCalls/projectsApi";
import {
  FETCH_ALL_PROJECTS_SUCCESS,
  FETCH_PROJECT_SUCCESS,
  FETCH_PROJECT_FAILURE,
  FETCH_PROJECT_REQUEST,
  UNSET_PROJECT
} from "../constants";

const fetchProjectRequest = () => ({
  type: FETCH_PROJECT_REQUEST
});

export const fetchProjectSuccess = (project) => ({
  type: FETCH_PROJECT_SUCCESS,
  payload: project
});

const fetchAllProjectSuccess = (projects) => ({
  type: FETCH_ALL_PROJECTS_SUCCESS,
  payload: projects
});

export const fetchProjectFailure = (error) => ({
  type: FETCH_PROJECT_FAILURE,
  payload: error
});

export const fetchProject = (id) => async (dispatch) => {
  dispatch(fetchProjectRequest());

  try {
    const res = await api.getProject(id);

    dispatch(fetchProjectSuccess(res.project));
  } catch (error) {
    dispatch(fetchProjectFailure(error.message));
  }
};

export const fetchProjects = () => async (dispatch) => {
  dispatch(fetchProjectRequest());

  try {
    const { projects } = await api.getProjects();

    dispatch(fetchAllProjectSuccess(projects));
  } catch (error) {
    dispatch(fetchProjectFailure(error.message));
  }
};

export const unsetProject = () => ({
  type: UNSET_PROJECT,
  payload: null
});