import {
  ERROR_MODAL_FALSE,
  ERROR_MODAL_TRUE,
  MODAL_LOGIN_OFF,
  MODAL_LOGIN_ON, PRIVATE_MODAL_OFF, PRIVATE_MODAL_ON
} from "../constants";

const initialState = {
  loginModal: false,
  privateModal: true,
  errorModal: false,
};

export const globals = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case MODAL_LOGIN_ON:
      return {
        ...state,
        loginModal: payload
      };
    case MODAL_LOGIN_OFF:
      return {
        ...state,
        loginModal: payload
      };
    case PRIVATE_MODAL_ON:
      return {
        ...state,
        privateModal: payload
      };
    case PRIVATE_MODAL_OFF:
      return {
        ...state,
        privateModal: payload
      };
    case ERROR_MODAL_TRUE:
      return {
        ...state,
        errorModal: payload
      };
    case ERROR_MODAL_FALSE:
      return {
        ...state,
        errorModal: payload
      };
    default:
      return state;
  }
};