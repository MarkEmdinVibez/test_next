import {
  MODAL_LOGIN_OFF,
  MODAL_LOGIN_ON,
  PRIVATE_MODAL_ON,
  PRIVATE_MODAL_OFF, ERROR_MODAL_TRUE, ERROR_MODAL_FALSE
} from "../constants";

export const modalLoginOn = () => ({
  type: MODAL_LOGIN_ON,
  payload: true
});
export const modalLoginOff = () => ({
  type: MODAL_LOGIN_OFF,
  payload: false
});

export const privateModalOn = () => ({
  type: PRIVATE_MODAL_ON,
  payload: true
});

export const privateModalOff = () => ({
  type: PRIVATE_MODAL_OFF,
  payload: false
});

export const errorModalOn = () => ({
  type: ERROR_MODAL_TRUE,
  payload: true
});
export const errorModalOff = () => ({
  type: ERROR_MODAL_FALSE,
  payload: false
});